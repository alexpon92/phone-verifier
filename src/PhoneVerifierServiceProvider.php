<?php
declare(strict_types=1);

namespace PhoneVerifier;

use Illuminate\Container\Container;
use Illuminate\Support\ServiceProvider;
use PhoneVerifier\Console\Commands\ClearVerificationLogs;
use PhoneVerifier\Domain\Factories\SmsVendors\SmsVendorsFactory;
use PhoneVerifier\Domain\Services\RenderTemplateServices\RenderTemplateService;
use PhoneVerifier\Domain\Services\Verification\SendVerificationSms;
use PhoneVerifier\Domain\Services\Verification\VerifyPhoneByCode;
use Psr\Log\LoggerInterface;

class PhoneVerifierServiceProvider extends ServiceProvider
{
    public function register()
    {
        if ($this->app->runningInConsole()) {
            $this->commands(
                [
                    ClearVerificationLogs::class
                ]
            );
        }

        $this->publishes(
            [
                __DIR__ . '/config/phone-verifier.php' => config_path('phone-verifier.php'),
            ],
            'config'
        );

        $this->publishes(
            [
                __DIR__ . '/Database/migrations' => base_path('database/migrations'),
            ],
            'migrations'
        );

        $this->mergeConfigFrom(
            __DIR__ . '/config/phone-verifier.php',
            'phone-verifier'
        );

        $this->app->bind(
            SendVerificationSms::class,
            static function ($app) {
                /** @var Container $app */
                return new SendVerificationSms(
                    (int)config('phone-verifier.limits.daily_ip_limit'),
                    (string)config('phone-verifier.sms_vendor_type'),
                    $app->make(SmsVendorsFactory::class),
                    (int)config('phone-verifier.code_length'),
                    (string)config('phone-verifier.sms_message_template'),
                    (string)config('phone-verifier.sms_sender_name'),
                    $app->make(RenderTemplateService::class),
                    $app->make(LoggerInterface::class)
                );
            }
        );

        $this->app->bind(
            SmsVendorsFactory::class,
            static function ($app) {
                /** @var Container $app */
                return new SmsVendorsFactory(
                    config('phone-verifier.sms_vendors')
                );
            }
        );

        $this->app->bind(
            VerifyPhoneByCode::class,
            static function ($app) {
                /** @var Container $app */
                return new VerifyPhoneByCode(
                    (string)config('phone-verifier.limits.available_life_time'),
                    (int)config('phone-verifier.limits.check_attempts_limit'),
                    $app->make(LoggerInterface::class)
                );
            }
        );
    }
}
