<?php

return [
    /**
     * default placeholders:
     * :code - verification code
     */
    'sms_message_template' => 'Verification code: :code',

    'code_length'     => 4,

    /**
     * Sms sender name - should be correct alpha-name for concrete service
     */
    'sms_sender_name' => 'Sender',

    /**
     * Available sms delivery service types:
     * smsc - sms center (RU)
     * kyiv_star - KyivStar (UA)
     */
    'sms_vendor_type' => 'smsc',

    'limits' => [
        'daily_ip_limit'       => 20,
        'resend_interval'      => 'PT60S',
        'check_attempts_limit' => 10,
        'available_life_time'  => 'PT10M'
    ],

    'sms_vendors' => [
        'smsc'      => [
            'login'    => env('PHONE_VERIFIER_SMSC_LOGIN'),
            'password' => env('PHONE_VERIFIER_SMSC_PWD'),
            'endpoint' => env('PHONE_VERIFIER_SMSC_ENDPOINT')
        ],
        'kyiv_star' => [
            'login'    => env('PHONE_VERIFIER_KYIV_STAR_LOGIN'),
            'password' => env('PHONE_VERIFIER_KYIV_STAR_PWD'),
            'endpoint' => env('PHONE_VERIFIER_KYIV_STAR_ENDPOINT')
        ],
        'bsg' => [
            'login'    => env('PHONE_VERIFIER_BSG_STAR_LOGIN', ''),
            'password' => env('PHONE_VERIFIER_BSG_STAR_PWD'),
            'endpoint' => env('PHONE_VERIFIER_BSG_STAR_ENDPOINT')
        ]
    ],

    'logs_life_time' => 'P5D',
];
