<?php
declare(strict_types=1);

namespace PhoneVerifier\Domain\Entity;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

/**
 * Class VerificationLog
 *
 * @package PhoneVerifier\Domain\Entity
 * @property string      $id
 * @property string      $phone
 * @property string      $ip
 * @property string      $code
 * @property string      $token
 * @property string      $status
 * @property string      $vendor_type
 * @property int         $attempts
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @mixin \Eloquent
 */
class VerificationLog extends Model
{
    public const STATUS_NOT_VERIFIED = 'not_verified';
    public const STATUS_VERIFIED     = 'verified';

    public static function countCurrentDayForIp(string $ip): int
    {
        return self::where('ip', $ip)
                   ->where('created_at', '>=', now()->startOfDay())
                   ->count('id');
    }

    public static function findLastSentFromDate(string $phone, Carbon $date): ?self
    {
        /** @var self $log */
        $log = self::where('phone', $phone)
                   ->where('created_at', '>=', $date)
                   ->first();
        return $log;
    }

    public static function findByPhoneAndToken(string $phone, string $token): ?self
    {
        /** @var self $log */
        $log = self::where('phone', $phone)
                   ->where('token', $token)
                   ->first();
        return $log;
    }

    public static function build(
        string $phone,
        string $ip,
        string $vendorType,
        string $code,
        string $token
    ): self {
        $log              = new VerificationLog();
        $log->phone       = $phone;
        $log->ip          = $ip;
        $log->vendor_type = $vendorType;
        $log->code        = $code;
        $log->token       = $token;
        $log->status      = self::STATUS_NOT_VERIFIED;
        $log->attempts    = 0;
        $log->save();
        return $log;
    }

    public function incrementAttempts(): void
    {
        ++$this->attempts;
        $this->save();
    }

    public static function clearLogs(Carbon $borderDate): void
    {
        VerificationLog::where('created_at', '<', $borderDate)
                       ->delete();
    }

    public function markAsVerified(): void
    {
        $this->status = self::STATUS_VERIFIED;
        $this->save();
    }

    public function isVerified(): bool
    {
        return $this->status === self::STATUS_VERIFIED;
    }
}