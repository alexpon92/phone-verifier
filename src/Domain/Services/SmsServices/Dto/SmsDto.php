<?php

declare(strict_types=1);

namespace PhoneVerifier\Domain\Services\SmsServices\Dto;

class SmsDto
{
    /**
     * @var string
     */
    private $vendorSmsId;

    /**
     * SmsDto constructor.
     * @param string $vendorSmsId
     */
    public function __construct(string $vendorSmsId)
    {
        $this->vendorSmsId = $vendorSmsId;
    }

    /**
     * @return string
     */
    public function getVendorSmsId(): string
    {
        return $this->vendorSmsId;
    }
}