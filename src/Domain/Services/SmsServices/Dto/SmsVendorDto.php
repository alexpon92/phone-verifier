<?php
declare(strict_types=1);

namespace PhoneVerifier\Domain\Services\SmsServices\Dto;

class SmsVendorDto
{
    /**
     * @var string
     */
    private $login;
    /**
     * @var string
     */
    private $password;
    /**
     * @var string
     */
    private $endpoint;

    /**
     * @var string
     */
    private $vendorType;

    /**
     * SmsVendorDto constructor.
     *
     * @param string $login
     * @param string $password
     * @param string $endpoint
     * @param string $vendorType
     */
    public function __construct(string $login, string $password, string $endpoint, string $vendorType)
    {
        $this->login      = $login;
        $this->password   = $password;
        $this->endpoint   = $endpoint;
        $this->vendorType = $vendorType;
    }

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getEndpoint(): string
    {
        return $this->endpoint;
    }

    /**
     * @return string
     */
    public function getVendorType(): string
    {
        return $this->vendorType;
    }
}