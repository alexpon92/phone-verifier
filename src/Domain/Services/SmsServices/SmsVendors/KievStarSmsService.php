<?php

declare(strict_types=1);

namespace PhoneVerifier\Domain\Services\SmsServices\SmsVendors;

use GuzzleHttp\Client;
use PhoneVerifier\Domain\Services\SmsServices\AbstractSmsService;
use PhoneVerifier\Domain\Services\SmsServices\Dto\SmsDto;
use PhoneVerifier\Domain\Services\SmsServices\Dto\SmsVendorDto;
use PhoneVerifier\Domain\Services\SmsServices\Exceptions\BadResponseCodeException;
use PhoneVerifier\Domain\Services\SmsServices\Exceptions\BadResponseFormatException;
use PhoneVerifier\Domain\Services\SmsServices\Exceptions\SendSmsException;
use Psr\Log\LoggerInterface;

class KievStarSmsService extends AbstractSmsService
{
    /**
     * @var Client
     */
    private $httpClient;

    /**
     * @var SmsVendorDto
     */
    private $smsVendorDto;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * KievStarSmsService constructor.
     *
     * @param Client          $httpClient
     * @param SmsVendorDto    $smsVendorDto
     * @param LoggerInterface $logger
     */
    public function __construct(Client $httpClient, SmsVendorDto $smsVendorDto, LoggerInterface $logger)
    {
        $config                = $httpClient->getConfig();
        $config['base_uri']    = $smsVendorDto->getEndpoint();
        $config['headers']     = [
            'Content-type'  => 'application/json',
            'Authorization' => base64_encode($smsVendorDto->getLogin() . ':' . $smsVendorDto->getPassword()),
        ];
        $config['http_errors'] = false;

        $this->httpClient   = new Client($config);
        $this->smsVendorDto = $smsVendorDto;
        $this->logger       = $logger;
    }

    protected function sendSms(string $smsText, string $phone, ?string $senderName): SmsDto
    {
        $payload = [
            'destination' => ltrim($phone, '+'),
            'bearerType'  => 'sms',
            'contentType' => 'text/plain',
            'serviceType' => '104',
            'content'     => $smsText,
        ];

        if ($senderName) {
            $payload['source'] = $senderName;
        }

        $response = $this->httpClient->post(
            'api/contents',
            [
                'json'    => $payload,
            ]
        );
        $content  = $response->getBody()->getContents();

        if ($response->getStatusCode() !== 202) {
            $this->logger->error(
                'Bad response http code from KievStart service',
                [
                    'code'    => $response->getStatusCode(),
                    'content' => $content
                ]
            );
            throw new BadResponseCodeException(
                "Bad response http code from KievStart service {$response->getStatusCode()}"
            );
        }

        $data = json_decode($content, true);

        if (!isset($data['mid'])) {
            $this->logger->error(
                'Bad response format from KievStar Service (no mid)',
                [
                    'content' => $content
                ]
            );

            throw new SendSmsException(
                'Bad response format from KievStar Service'
            );
        }

        if (isset($data['errorId'], $data['errorMsg'])) {
            $this->logger->error(
                'Error response from KievStar Service',
                [
                    'content' => $content
                ]
            );

            throw new BadResponseFormatException(
                'Error response from KievStar Service'
            );
        }

        return new SmsDto((string)$data['mid']);
    }

    public function getLogger(): LoggerInterface
    {
        return $this->logger;
    }

    public function getSmsVendor(): SmsVendorDto
    {
        return $this->smsVendorDto;
    }
}