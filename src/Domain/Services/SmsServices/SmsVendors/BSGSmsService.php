<?php
declare(strict_types=1);

namespace PhoneVerifier\Domain\Services\SmsServices\SmsVendors;

use PhoneVerifier\Domain\Services\SmsServices\AbstractSmsService;
use PhoneVerifier\Domain\Services\SmsServices\Dto\SmsVendorDto;
use PhoneVerifier\Domain\Services\SmsServices\Dto\SmsDto;
use PhoneVerifier\Domain\Services\SmsServices\Exceptions\BadResponseCodeException;
use PhoneVerifier\Domain\Services\SmsServices\Exceptions\BadResponseFormatException;
use PhoneVerifier\Domain\Services\SmsServices\Exceptions\SendSmsException;
use GuzzleHttp\Client;
use Illuminate\Support\Str;
use Psr\Log\LoggerInterface;

class BSGSmsService extends AbstractSmsService
{
    /**
     * @var SmsVendorDto
     */
    private $smsVendorDto;

    /**
     * @var Client
     */
    private $httpClient;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * BSGSmsService constructor.
     * @param Client          $httpClient
     * @param SmsVendorDto    $smsVendorDto
     * @param LoggerInterface $logger
     */
    public function __construct(Client $httpClient, SmsVendorDto $smsVendorDto, LoggerInterface $logger)
    {
        $config                = $httpClient->getConfig();
        $config['base_uri']    = $smsVendorDto->getEndpoint();
        $config['http_errors'] = false;

        $this->httpClient   = new Client($config);
        $this->smsVendorDto = $smsVendorDto;
        $this->logger       = $logger;
    }

    protected function sendSms(string $smsText, string $phone, ?string $senderName): SmsDto
    {
        $payload = [
            'destination' => 'phone',
            'body'        => $smsText,
            'msisdn'      => $phone,
            'reference'   => $this->generateUniqueId(),
        ];

        if (!$senderName) {
            $this->logger->warning('Sender name is not set for BSG sms provider');
            throw new SendSmsException('Sender name is not set for BSG sms provider');
        }

        $payload['originator'] = $senderName;

        $response = $this->httpClient->post(
            'rest/sms/create',
            [
                'headers' => [
                    'X-API-KEY' => $this->smsVendorDto->getPassword(),
                ],
                'json'    => $payload,
            ]
        );

        $content = $response->getBody()->getContents();

        $this->logger->debug(
            'BSG response on send sms',
            [
                'code'    => $response->getStatusCode(),
                'content' => $content,
            ]
        );

        if ($response->getStatusCode() !== 200) {
            $this->logger->error(
                'Bad response http code from BSG service',
                [
                    'code'    => $response->getStatusCode(),
                    'content' => $content,
                ]
            );
            throw new BadResponseCodeException(
                "Bad response http code from BSG service {$response->getStatusCode()}"
            );
        }

        $data = json_decode($content, true);

        if (isset($data['result']['error'], $data['result']['reference']) && (int)$data['result']['error'] === 0) {
            return new SmsDto($data['result']['reference']);
        }

        $this->logger->debug(
            'Bad response format from BSG Service (no reference or error)',
            [
                'code'    => $response->getStatusCode(),
                'content' => $content,
            ]
        );

        throw new BadResponseFormatException('Bad response format from BSG service');
    }

    public function getLogger(): LoggerInterface
    {
        return $this->logger;
    }

    public function getSmsVendor(): SmsVendorDto
    {
        return $this->smsVendorDto;
    }

    private function generateUniqueId(): string
    {
        return Str::random();
    }
}
