<?php
declare(strict_types=1);

namespace PhoneVerifier\Domain\Services\SmsServices\SmsVendors;

use GuzzleHttp\Client;
use PhoneVerifier\Domain\Services\SmsServices\AbstractSmsService;
use PhoneVerifier\Domain\Services\SmsServices\Dto\SmsDto;
use PhoneVerifier\Domain\Services\SmsServices\Dto\SmsVendorDto;
use PhoneVerifier\Domain\Services\SmsServices\Exceptions\BadResponseCodeException;
use PhoneVerifier\Domain\Services\SmsServices\Exceptions\BadResponseFormatException;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;

class CoreSmsService extends AbstractSmsService
{
    /**
     * @var Client
     */
    private $httpClient;

    /**
     * @var SmsVendorDto
     */
    private $smsVendorDto;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * CoreSmsService constructor.
     * @param Client          $httpClient
     * @param SmsVendorDto    $smsVendorDto
     * @param LoggerInterface $logger
     */
    public function __construct(Client $httpClient, SmsVendorDto $smsVendorDto, LoggerInterface $logger)
    {
        $config                = $httpClient->getConfig();
        $config['base_uri']    = $smsVendorDto->getEndpoint();
        $config['http_errors'] = false;
        $config['headers']     = [
            'auth-token'   => $smsVendorDto->getPassword(),
            'Landing'      => $smsVendorDto->getLogin(),
            'Accept'       => 'application/json',
            'Content-type' => 'application/json',
        ];

        $this->httpClient   = new Client($config);
        $this->smsVendorDto = $smsVendorDto;
        $this->logger       = $logger;
    }

    protected function sendSms(string $smsText, string $phone, ?string $senderName): SmsDto
    {
        $payload = [
            'phone' => $phone,
            'text'  => $smsText,
        ];

        $response = $this->httpClient->post(
            'landings/send-sms',
            [
                'json' => $payload,
            ]
        );

        $content = $response->getBody()->getContents();

        $this->logger->debug(
            'CORE response on send sms',
            [
                'code'    => $response->getStatusCode(),
                'content' => $content,
            ]
        );

        if ($response->getStatusCode() !== 200) {
            $this->logger->debug(
                'Bad response http code from CORE service',
                [
                    'code'    => $response->getStatusCode(),
                    'content' => $content,
                ]
            );
            throw new BadResponseCodeException(
                "Bad response http code from CORE service {$response->getStatusCode()}"
            );
        }

        $data = json_decode($content, true);

        if (!isset($data['success']) || $data['success'] !== true) {
            $this->logger->debug(
                'Error on sending SMS to CORE. No ID or error.',
                [
                    'code'    => $response->getStatusCode(),
                    'content' => $content,
                ]
            );

            throw new BadResponseFormatException('Error on send SMS via CORE');
        }

        return new SmsDto($this->generateUniqueId());
    }

    public function getLogger(): LoggerInterface
    {
        return $this->logger;
    }

    private function generateUniqueId(): string
    {
        return Uuid::uuid4()->toString();
    }

    public function getSmsVendor(): SmsVendorDto
    {
        return $this->smsVendorDto;
    }

}
