<?php
declare(strict_types=1);

namespace PhoneVerifier\Domain\Services\SmsServices;

class SmsVendorsTypeEnum
{
    public const SMSC      = 'smsc';
    public const KYIV_STAR = 'kyiv_star';
    public const BSG       = 'bsg';
    public const CORE      = 'core';
}