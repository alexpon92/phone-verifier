<?php
declare(strict_types=1);

namespace PhoneVerifier\Domain\Services\SmsServices\Exceptions;

class BadResponseFormatException extends \Exception
{

}