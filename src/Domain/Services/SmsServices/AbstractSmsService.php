<?php
declare(strict_types=1);

namespace PhoneVerifier\Domain\Services\SmsServices;

use PhoneVerifier\Domain\Services\SmsServices\Dto\SmsDto;
use PhoneVerifier\Domain\Services\SmsServices\Dto\SmsVendorDto;
use PhoneVerifier\Domain\Services\SmsServices\Exceptions\BadResponseCodeException;
use PhoneVerifier\Domain\Services\SmsServices\Exceptions\BadResponseFormatException;
use PhoneVerifier\Domain\Services\SmsServices\Exceptions\SendSmsException;
use Psr\Log\LoggerInterface;

abstract class AbstractSmsService
{
    /**
     * @param string      $smsText
     * @param string      $phone
     * @param string|null $senderName
     *
     * @return SmsDto
     * @throws BadResponseCodeException
     * @throws BadResponseFormatException
     * @throws SendSmsException
     */
    abstract protected function sendSms(string $smsText, string $phone, ?string $senderName): SmsDto;

    abstract public function getLogger(): LoggerInterface;

    abstract public function getSmsVendor(): SmsVendorDto;

    /**
     * @param string      $smsText
     * @param string      $phone
     * @param string|null $senderName
     *
     * @return SmsDto
     * @throws SendSmsException
     */
    final public function send(string $smsText, string $phone, ?string $senderName): SmsDto
    {
        try {
            $smsDto = $this->sendSms($smsText, $phone, $senderName);
        } catch (\Exception $exception) {
            $this->getLogger()->error(
                'Sms send error',
                [
                    'exception'       => $exception->getMessage(),
                    'sms_vendor_name' => $this->getSmsVendor()->getVendorType(),
                    'phone'           => $phone
                ]
            );
            throw new SendSmsException('Sms Send error', 0, $exception);
        }

        return $smsDto;
    }
}