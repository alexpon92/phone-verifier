<?php
declare(strict_types=1);

namespace PhoneVerifier\Domain\Services\Ip;

class IpResolver
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return string
     */
    public function getIp($request): ?string
    {
        if ($request->header('CF-Connecting-IP')) {
            return $request->header('CF-Connecting-IP');
        }

        if ($request->header('X-Forwarded-For')) {
            return $request->header('X-Forwarded-For');
        }

        if ($request->ip()) {
            return $request->ip();
        }

        return null;
    }
}