<?php
declare(strict_types=1);

namespace PhoneVerifier\Domain\Services\RenderTemplateServices;

class RenderTemplateService
{
    public function renderTemplate(string $templateText, array $placeholders): string
    {
        foreach ($placeholders as $key => $value) {
            $templateText = str_replace(':' . $key, $value, $templateText);
        }

        return $templateText;
    }
}