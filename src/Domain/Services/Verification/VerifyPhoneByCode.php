<?php
declare(strict_types=1);

namespace PhoneVerifier\Domain\Services\Verification;

use PhoneVerifier\Domain\Entity\VerificationLog;
use Psr\Log\LoggerInterface;

class VerifyPhoneByCode
{
    /**
     * @var string
     */
    private $availableLifeTime;

    /**
     * @var int
     */
    private $maxAttempts;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * VerifyPhoneByCode constructor.
     *
     * @param string          $availableLifeTime
     * @param int             $maxAttempts
     * @param LoggerInterface $logger
     */
    public function __construct(string $availableLifeTime, int $maxAttempts, LoggerInterface $logger)
    {
        $this->availableLifeTime = $availableLifeTime;
        $this->maxAttempts       = $maxAttempts;
        $this->logger            = $logger;
    }

    /**
     * @param string $phone
     * @param string $code
     * @param string $token
     *
     * @return bool
     */
    public function execute(string $phone, string $code, string $token): bool
    {
        $log = VerificationLog::findByPhoneAndToken($phone, $token);
        if (!$log) {
            return false;
        }

        if ($log->created_at->add(new \DateInterval($this->availableLifeTime)) < now()) {
            return false;
        }

        if ($log->attempts > $this->maxAttempts) {
            return false;
        }

        if ($log->isVerified()) {
            return false;
        }

        $log->incrementAttempts();

        if ($log->code === $code) {
            $log->markAsVerified();
            return true;
        }

        return false;
    }
}