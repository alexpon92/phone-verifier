<?php
declare(strict_types=1);

namespace PhoneVerifier\Domain\Services\Verification;

use PhoneVerifier\Domain\Entity\VerificationLog;
use PhoneVerifier\Domain\Services\RenderTemplateServices\RenderTemplateService;
use PhoneVerifier\Domain\Services\Verification\Exceptions\DailyLimitExceededException;
use PhoneVerifier\Domain\Services\Verification\Exceptions\SmsDeliveryException;
use PhoneVerifier\Domain\Factories\SmsVendors\Exceptions\BadSmsVendorConfigException;
use PhoneVerifier\Domain\Factories\SmsVendors\Exceptions\UndefinedSmsVendorTypeException;
use PhoneVerifier\Domain\Factories\SmsVendors\SmsVendorsFactory;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;

class SendVerificationSms
{
    /**
     * @var int
     */
    private $dailyIpLimit;

    /**
     * @var string
     */
    private $smsVendorType;

    /**
     * @var SmsVendorsFactory
     */
    private $vendorsFactory;

    /**
     * @var int
     */
    private $codeLength;

    /**
     * @var string
     */
    private $smsTemplate;

    /**
     * @var string|null
     */
    private $senderName;

    /**
     * @var RenderTemplateService
     */
    private $renderTemplateService;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * SendVerificationSms constructor.
     * @param int                   $dailyIpLimit
     * @param string                $smsVendorType
     * @param SmsVendorsFactory     $vendorsFactory
     * @param int                   $codeLength
     * @param string                $smsTemplate
     * @param string|null           $senderName
     * @param RenderTemplateService $renderTemplateService
     * @param LoggerInterface       $logger
     */
    public function __construct(
        int $dailyIpLimit,
        string $smsVendorType,
        SmsVendorsFactory $vendorsFactory,
        int $codeLength,
        string $smsTemplate,
        ?string $senderName,
        RenderTemplateService $renderTemplateService,
        LoggerInterface $logger
    ) {
        $this->dailyIpLimit          = $dailyIpLimit;
        $this->smsVendorType         = $smsVendorType;
        $this->vendorsFactory        = $vendorsFactory;
        $this->codeLength            = $codeLength;
        $this->smsTemplate           = $smsTemplate;
        $this->senderName            = $senderName;
        $this->renderTemplateService = $renderTemplateService;
        $this->logger                = $logger;
    }

    /**
     * @param string     $phone
     * @param string     $ip
     * @param array|null $placeHolders
     *
     * @return VerificationLog
     * @throws DailyLimitExceededException
     * @throws SmsDeliveryException
     * @throws BadSmsVendorConfigException
     * @throws UndefinedSmsVendorTypeException
     */
    public function execute(string $phone, string $ip, ?array $placeHolders = []): VerificationLog
    {
        $count = VerificationLog::countCurrentDayForIp(
            $ip
        );

        if ($count > $this->dailyIpLimit) {
            $this->logger->warning(
                'Daily verification limit exceeded for phone',
                [
                    'phone',
                ]
            );
            throw new DailyLimitExceededException("Daily verification limit exceeded for phone {$phone}");
        }

        $recentlySent = VerificationLog::findLastSentFromDate($phone, now()->subMinute());
        if ($recentlySent) {
            return $recentlySent;
        }

        $smsService = $this->vendorsFactory->createByType($this->smsVendorType);

        $code                 = $this->generateCode($this->codeLength);
        $placeHolders['code'] = $code;

        $text = $this->renderTemplate($this->smsTemplate, $placeHolders);

        try {
            $smsService->send($text, $phone, $this->senderName);
        } catch (\Exception $e) {
            throw new SmsDeliveryException('Error while sending verification sms', 0, $e);
        }

        return VerificationLog::build(
            $phone,
            $ip,
            $this->smsVendorType,
            $code,
            $this->generateToken()
        );
    }

    private function renderTemplate(string $templateText, array $placeholders): string
    {
        return $this->renderTemplateService->renderTemplate($templateText, $placeholders);
    }

    private function generateCode(int $length): string
    {
        $leadSymbol = random_int(0, 9);

        $min = 10 ** ($length - 2);
        $max = 10 ** ($length - 1) - 1;
        return $leadSymbol . random_int($min, $max);
    }

    private function generateToken(): string
    {
        return Uuid::uuid4()->toString();
    }
}
