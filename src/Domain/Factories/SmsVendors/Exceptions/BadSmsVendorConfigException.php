<?php
declare(strict_types=1);

namespace PhoneVerifier\Domain\Factories\SmsVendors\Exceptions;

class BadSmsVendorConfigException extends \Exception
{

}