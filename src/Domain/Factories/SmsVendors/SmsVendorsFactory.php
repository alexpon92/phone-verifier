<?php
declare(strict_types=1);

namespace PhoneVerifier\Domain\Factories\SmsVendors;

use GuzzleHttp\Client;
use PhoneVerifier\Domain\Services\SmsServices\AbstractSmsService;
use PhoneVerifier\Domain\Services\SmsServices\Dto\SmsVendorDto;
use PhoneVerifier\Domain\Services\SmsServices\SmsVendors\BSGSmsService;
use PhoneVerifier\Domain\Services\SmsServices\SmsVendors\CoreSmsService;
use PhoneVerifier\Domain\Services\SmsServices\SmsVendors\KievStarSmsService;
use PhoneVerifier\Domain\Services\SmsServices\SmsVendors\SMSCSmsService;
use PhoneVerifier\Domain\Services\SmsServices\SmsVendorsTypeEnum;
use PhoneVerifier\Domain\Factories\SmsVendors\Exceptions\BadSmsVendorConfigException;
use PhoneVerifier\Domain\Factories\SmsVendors\Exceptions\UndefinedSmsVendorTypeException;
use Psr\Log\LoggerInterface;

class SmsVendorsFactory
{
    /**
     * @var array
     */
    private $config;

    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * @param string $type
     *
     * @return AbstractSmsService
     * @throws BadSmsVendorConfigException
     * @throws UndefinedSmsVendorTypeException
     */
    public function createByType(string $type): AbstractSmsService
    {
        if (empty($this->config) || !isset($this->config[$type]['login'], $this->config[$type]['password'], $this->config[$type]['endpoint'])) {
            throw new BadSmsVendorConfigException("Bad sms vendor config for vendor type: {$type}");
        }

        $smsVendorDto = new SmsVendorDto(
            $this->config[$type]['login'],
            $this->config[$type]['password'],
            $this->config[$type]['endpoint'],
            $type
        );

        switch ($type) {
            case SmsVendorsTypeEnum::SMSC:
                return new SMSCSmsService(
                    app(Client::class),
                    $smsVendorDto,
                    app(LoggerInterface::class)
                );
            case SmsVendorsTypeEnum::KYIV_STAR:
                return new KievStarSmsService(
                    app(Client::class),
                    $smsVendorDto,
                    app(LoggerInterface::class)
                );
            case SmsVendorsTypeEnum::BSG:
                return new BSGSmsService(
                    app(Client::class),
                    $smsVendorDto,
                    app(LoggerInterface::class)
                );
            case SmsVendorsTypeEnum::CORE:
                return new CoreSmsService(
                    app(Client::class),
                    $smsVendorDto,
                    app(LoggerInterface::class)
                );
            default:
                throw new UndefinedSmsVendorTypeException("Undefined sns vendor type {$type}");
        }
    }
}
