<?php

namespace PhoneVerifier\Console\Commands;

use Illuminate\Console\Command;
use PhoneVerifier\Domain\Entity\VerificationLog;

class ClearVerificationLogs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'phone-verifier:clear-logs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear verification logs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        VerificationLog::clearLogs(now()->sub(new \DateInterval(config('phone-verifier.logs_life_time'))));
    }
}
