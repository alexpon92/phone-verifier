<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class VerificationLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('verification_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('phone', 50)->index();
            $table->string('vendor_type', 50);
            $table->string('ip', 50)->index();
            $table->string('code', 10);
            $table->string('token', 100);
            $table->string('status', 100);
            $table->integer('attempts')->default(0);
            $table->timestamps();

            $table->unique(['phone', 'token']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('verification_logs');
    }
}
