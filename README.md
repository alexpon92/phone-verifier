To install package require it through composer.

1. Register ```PhoneVerifier\PhoneVerifierServiceProvider```
2. Publish config ```php artisan vendor:publish --provider="PhoneVerifier\PhoneVerifierServiceProvider" --tag=config```
2. Publish config ```php artisan vendor:publish --provider="PhoneVerifier\PhoneVerifierServiceProvider" --tag=migrations```
3. Set params for sms vendors in your .env